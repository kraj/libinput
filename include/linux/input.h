#ifdef __linux__
#include "linux/input.h"
#elif __FreeBSD__
#include "freebsd/input.h"
#endif

#ifndef input_event_sec
#define input_event_sec time.tv_sec
#define input_event_usec time.tv_usec
#endif
